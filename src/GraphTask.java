import java.time.Duration;
import java.time.Instant;
import java.util.*;

/**
 * Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

    /**
     * Main method.
     */
    public static void main(String[] args) {
        GraphTask a = new GraphTask();
        a.run();
    }

    /**
     * Actual main method to run examples and everything.
     * <p>
     * EE - Koostada meetod etteantud lihtgraafi kolmnurkade graafi moodustamiseks.
     * EN - Create a method for forming a triangle graph of a given simple graph.
     */
    public void run() {

        // Vertexes
        int testLargeVertexes = 2000;
        // Arcs
        int testLargeArcs = 4000;

        Instant start;
        Instant end;

        start = Instant.now();
        firstGraph();
        end = Instant.now();
        System.out.println("3 Vertexes, 3 edges took " + Duration.between(start, end).getNano() + " nanoseconds");

        start = Instant.now();
        secondGraph();
        end = Instant.now();
        System.out.println("5 Vertexes, 7 edges took " + Duration.between(start, end).getNano() + " nanoseconds");

        start = Instant.now();
        thirdGraph();
        end = Instant.now();
        System.out.println("8 Vertexes, 10 edges took " + Duration.between(start, end).getNano() + " nanoseconds");

        start = Instant.now();
        fourthGraph();
        end = Instant.now();
        System.out.println("6 Vertexes, 9 edges took " + Duration.between(start, end).getNano() + " nanoseconds");

        start = Instant.now();
        fifthGraph(testLargeVertexes, testLargeArcs);
        end = Instant.now();
        System.out.println(testLargeVertexes + " Vertexes, " + testLargeArcs + " edges took " + Duration.between(start, end).getNano() + " nanoseconds");

    }

    /**
     * Simple triangle, supposed to return same triangle with connected edges
     */
    public void firstGraph() {

        Graph g = new Graph("G1");
        Vertex v1 = new Vertex("v1");
        Vertex v2 = new Vertex("v2");
        Vertex v3 = new Vertex("v3");
        v1.next = v2;
        v2.next = v3;
        g.first = v1;
        g.createArc(v1.id + "_" + v2.id, v1, v2);
        g.createArc(v2.id + "_" + v1.id, v2, v1);
        g.createArc(v1.id + "_" + v3.id, v1, v3);
        g.createArc(v3.id + "_" + v1.id, v3, v1);
        g.createArc(v2.id + "_" + v3.id, v2, v3);
        g.createArc(v3.id + "_" + v2.id, v3, v2);
        System.out.println(g.findCliques("G1 Triangles"));
    }

    /**
     * Triangle with added vertex to form a diamond shape (two triangles mirrored)
     */
    public void secondGraph() {
        Graph g = new Graph("G2");
        Vertex v1 = new Vertex("v1");
        Vertex v2 = new Vertex("v2");
        Vertex v3 = new Vertex("v3");
        Vertex v4 = new Vertex("v4");
        Vertex v5 = new Vertex("v5");
        v1.next = v2;
        v2.next = v3;
        v3.next = v4;
        v4.next = v5;
        g.first = v1;
        g.createArc(v1.id + "_" + v2.id, v1, v2);
        g.createArc(v2.id + "_" + v1.id, v2, v1);
        g.createArc(v1.id + "_" + v3.id, v1, v3);
        g.createArc(v3.id + "_" + v1.id, v3, v1);
        g.createArc(v2.id + "_" + v3.id, v2, v3);
        g.createArc(v3.id + "_" + v2.id, v3, v2);
        g.createArc(v3.id + "_" + v4.id, v3, v4);
        g.createArc(v4.id + "_" + v3.id, v4, v3);
        g.createArc(v2.id + "_" + v4.id, v2, v4);
        g.createArc(v4.id + "_" + v2.id, v4, v2);
        g.createArc(v4.id + "_" + v5.id, v4, v5);
        g.createArc(v5.id + "_" + v4.id, v5, v4);
        g.createArc(v2.id + "_" + v5.id, v2, v5);
        g.createArc(v5.id + "_" + v2.id, v5, v2);
        System.out.println(g.findCliques("G2 Triangles"));
    }

    /**
     * Previous two triangles mirrored with added vertexes
     * v3 to v6 , v4 to v7 and v7 to v8
     * Should return same result as previous test because vertexes 6,7 and 8 are not connected to form a triangle
     */
    public void thirdGraph() {
        Graph g = new Graph("G3");
        Vertex v1 = new Vertex("v1");
        Vertex v2 = new Vertex("v2");
        Vertex v3 = new Vertex("v3");
        Vertex v4 = new Vertex("v4");
        Vertex v5 = new Vertex("v5");
        Vertex v6 = new Vertex("v6");
        Vertex v7 = new Vertex("v7");
        Vertex v8 = new Vertex("v8");
        v1.next = v2;
        v2.next = v3;
        v3.next = v4;
        v4.next = v5;
        v5.next = v6;
        v6.next = v7;
        v7.next = v8;
        g.first = v1;
        g.createArc(v1.id + "_" + v2.id, v1, v2);
        g.createArc(v2.id + "_" + v1.id, v2, v1);
        g.createArc(v1.id + "_" + v3.id, v1, v3);
        g.createArc(v3.id + "_" + v1.id, v3, v1);
        g.createArc(v2.id + "_" + v3.id, v2, v3);
        g.createArc(v3.id + "_" + v2.id, v3, v2);
        g.createArc(v3.id + "_" + v4.id, v3, v4);
        g.createArc(v4.id + "_" + v3.id, v4, v3);
        g.createArc(v2.id + "_" + v4.id, v2, v4);
        g.createArc(v4.id + "_" + v2.id, v4, v2);
        g.createArc(v4.id + "_" + v5.id, v4, v5);
        g.createArc(v5.id + "_" + v4.id, v5, v4);
        g.createArc(v2.id + "_" + v5.id, v2, v5);
        g.createArc(v5.id + "_" + v2.id, v5, v2);

        g.createArc(v3.id + "_" + v6.id, v3, v6);
        g.createArc(v6.id + "_" + v3.id, v6, v3);
        g.createArc(v4.id + "_" + v7.id, v4, v7);
        g.createArc(v7.id + "_" + v4.id, v7, v4);
        g.createArc(v7.id + "_" + v8.id, v7, v8);
        g.createArc(v8.id + "_" + v7.id, v8, v7);
        System.out.println(g.findCliques("G3 Triangles"));
    }

    /**
     * Random graph with few vertexes and edges
     */
    public void fourthGraph() {
        Graph g1 = new Graph("G4");
        g1.createRandomSimpleGraph(6, 9);
        System.out.println(g1.findCliques("G4 Triangles"));
    }

    /**
     * Random graph with many vertexes and edges
     *
     * @param v number of vertexes
     * @param a number of edges
     */
    public void fifthGraph(int v, int a) {
        Graph g1 = new Graph("G5");
        g1.createRandomSimpleGraph(v, a);
        System.out.println(g1.findCliques("G5 Triangles"));
    }

    /**
     * Vertex is a single point in a graph. Vertexes can be linked with next property.
     * Also each vertex can have unlimited arcs. Each arc has to connect to another vertex.
     */
    class Vertex {

        private String id;
        private Vertex next;
        private Arc first;
        private int info = 0;

        Vertex(String s, Vertex v, Arc e) {
            id = s;
            next = v;
            first = e;
        }

        Vertex(String s) {
            this(s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }

        /**
         * @param o input Vertex
         * @return a negative integer, zero, or a positive integer as this object name is less than, equal to, or greater than the specified object.
         */
        public int compareTo(Vertex o) {
            return id.compareTo(o.id);
        }

    }


    /**
     * Arc represents one arrow in the graph. Two-directional edges are
     * represented by two Arc objects (for both directions).
     */
    class Arc {

        private String id;
        private Vertex target;
        private Arc next;
        private int info = 0;

        Arc(String s, Vertex v, Arc a) {
            id = s;
            target = v;
            next = a;
        }

        Arc(String s) {
            this(s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }
    }

    /**
     * Graph is a collection of vertexes. Vertexes can be connected with edges.
     */
    class Graph {

        private String id;
        private Vertex first;
        private int info = 0;

        Graph(String s, Vertex v) {
            id = s;
            first = v;
        }

        Graph(String s) {
            this(s, null);
        }

        @Override
        public String toString() {
            String nl = System.getProperty("line.separator");
            StringBuilder sb = new StringBuilder(nl);
            sb.append(id);
            sb.append(nl);
            Vertex v = first;
            while (v != null) {
                sb.append(v);
                sb.append(" -->");
                Arc a = v.first;
                while (a != null) {
                    sb.append(" ");
                    sb.append(a);
                    sb.append(" (");
                    sb.append(v);
                    sb.append("->");
                    sb.append(a.target.toString());
                    sb.append(")");
                    a = a.next;
                }
                sb.append(nl);
                v = v.next;
            }
            return sb.toString();
        }

        /**
         * Creates a Vertex
         *
         * @param vid vertex name
         * @return a new Vertex
         */
        public Vertex createVertex(String vid) {
            Vertex res = new Vertex(vid);
            res.next = first;
            first = res;
            return res;
        }

        /**
         * Creates an arc
         *
         * @param aid  arc name
         * @param from origination vertex
         * @param to   destination vertex
         * @return a new Arc
         */
        public Arc createArc(String aid, Vertex from, Vertex to) {
            Arc res = new Arc(aid);
            res.next = from.first;
            from.first = res;
            res.target = to;
            return res;
        }

        /**
         * Create a connected undirected random tree with n vertices.
         * Each new vertex is connected to some random existing vertex.
         *
         * @param n number of vertices added to this graph
         */
        public void createRandomTree(int n) {
            if (n <= 0)
                return;
            Vertex[] varray = new Vertex[n];
            for (int i = 0; i < n; i++) {
                varray[i] = createVertex("v" + (n - i));
                if (i > 0) {
                    int vnr = (int) (Math.random() * i);
                    createArc("a" + varray[vnr].toString() + "_"
                            + varray[i].toString(), varray[vnr], varray[i]);
                    createArc("a" + varray[i].toString() + "_"
                            + varray[vnr].toString(), varray[i], varray[vnr]);
                }
            }
        }

        /**
         * Create an adjacency matrix of this graph.
         * Side effect: corrupts info fields in the graph
         *
         * @return adjacency matrix
         */
        public int[][] createAdjMatrix() {
            info = 0;
            Vertex v = first;
            while (v != null) {
                v.info = info++;
                v = v.next;
            }
            int[][] res = new int[info][info];
            v = first;
            while (v != null) {
                int i = v.info;
                Arc a = v.first;
                while (a != null) {
                    int j = a.target.info;
                    res[i][j]++;
                    a = a.next;
                }
                v = v.next;
            }
            return res;
        }

        /**
         * Create a connected simple (undirected, no loops, no multiple
         * arcs) random graph with n vertices and m edges.
         *
         * @param n number of vertices
         * @param m number of edges
         */
        public void createRandomSimpleGraph(int n, int m) {
            if (n <= 0)
                return;
//            if (n > 2500)
//                throw new IllegalArgumentException("Too many vertices: " + n);
            if (m < n - 1 || m > n * (n - 1) / 2)
                throw new IllegalArgumentException
                        ("Impossible number of edges: " + m);
            first = null;
            createRandomTree(n);       // n-1 edges created here
            Vertex[] vert = new Vertex[n];
            Vertex v = first;
            int c = 0;
            while (v != null) {
                vert[c++] = v;
                v = v.next;
            }
            int[][] connected = createAdjMatrix();
            int edgeCount = m - n + 1;  // remaining edges
            while (edgeCount > 0) {
                int i = (int) (Math.random() * n);  // random source
                int j = (int) (Math.random() * n);  // random target
                if (i == j)
                    continue;  // no loops
                if (connected[i][j] != 0 || connected[j][i] != 0)
                    continue;  // no multiple edges
                Vertex vi = vert[i];
                Vertex vj = vert[j];
                createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
                connected[i][j] = 1;
                createArc("a" + vj + "_" + vi, vj, vi);
                connected[j][i] = 1;
                edgeCount--;  // a new edge happily created
            }
        }

        /**
         * @param id  Vertex to find
         * @param arr array of vertexes
         * @return returns the vertex with the input vertex name
         */
        public Vertex findVertexById(Vertex id, Vertex[] arr) {

            for (Vertex v : arr) {
                if (v.id.equals(id.id)) {
                    return v;
                }
            }
            return null;
        }

        /**
         * Create a HashSet of unique vertexes
         *
         * @param triangles Hashmap of vertex name combinations and corresponding array of vertexes
         * @return HashSet of unique vertexes
         */
        public HashSet<Vertex> createUniqueIdList(HashMap<String, Vertex[]> triangles) {

            HashSet<Vertex> uniqueVertexes = new HashSet<>();
            for (Map.Entry<String, Vertex[]> entry : triangles.entrySet()) {
                Vertex[] value = entry.getValue();
                uniqueVertexes.addAll(Arrays.asList(value));
            }
            return uniqueVertexes;
        }

        /**
         * @param name           graph name
         * @param uniqueVertexes HashSet of unique vertexes
         * @param triangles      Hashmap of vertex name combinations and corresponding array of vertexes
         * @return A Graph with interconnected triangles.
         */
        public Graph createCliquesGraph(String name, HashSet<Vertex> uniqueVertexes, HashMap<String, Vertex[]> triangles) {

            // create graph and add vertexes into a vertex list
            Graph res = new Graph(name);
            Vertex[] vert = new Vertex[uniqueVertexes.size()];

            int counter = 0;
            for (Vertex uniqueVertex : uniqueVertexes) {
                Vertex v = new Vertex(uniqueVertex.id);
                vert[counter] = v;
                counter++;
            }

            // Add references to vertexes in the list and finally reference the first vertex
            for (int i = 0; i < vert.length - 1; i++) {
                vert[i].next = vert[i + 1];
            }
            res.first = vert[0];

            // combinations for vertex names added to already created arc names
            List<String> arcNames = new ArrayList<>();
            int[][] trips = {
                    {0, 1},
                    {2, 0},
                    {1, 2}
            };

            // create arcs and add created arc names to list so there are no duplicates
            for (Map.Entry<String, Vertex[]> entry : triangles.entrySet()) {
                Vertex[] value = entry.getValue();

                for (int i = 0; i < 3; i++) {

                    if (!arcNames.contains("a" + value[trips[i][0]] + "_" + value[trips[i][1]]) && !arcNames.contains("a" + value[trips[i][0]] + "_" + value[trips[i][1]])) {

                        res.createArc("a" + value[trips[i][0]] + "_" + value[trips[i][1]], findVertexById(value[trips[i][0]], vert), findVertexById(value[trips[i][1]], vert));
                        res.createArc("a" + value[trips[i][1]] + "_" + value[trips[i][0]], findVertexById(value[trips[i][1]], vert), findVertexById(value[trips[i][0]], vert));
                        arcNames.add("a" + value[trips[i][0]] + "_" + value[trips[i][1]]);
                        arcNames.add("a" + value[trips[i][1]] + "_" + value[trips[i][0]]);

                    }
                }
            }
            return res;
        }

        /**
         * Finds all triple interconnected vertexes and adds them into a HashMap in a sorted manner.
         *
         * @return Hashmap of vertex name combinations as keys and corresponding array of vertexes as values
         */
        public HashMap<String, Vertex[]> findTriangleSets() {

            HashMap<String, Vertex[]> triangles = new HashMap<>();

            Vertex v1 = first;
            while (v1 != null) {

                Arc a = v1.first;
                while (a != null) {

                    Vertex v2 = a.target;
                    if (v2 != null) {

                        Arc a2 = v2.first;
                        while (a2 != null) {
                            if (a2.target != v1) {

                                Vertex v3 = a2.target;
                                if (v3 != null) {

                                    Arc a3 = v3.first;
                                    while (a3 != null) {
                                        if (a3.target != v2) {
                                            if (a3.target == v1) {

                                                Vertex[] sortedVertexNames = new Vertex[]{v1, v2, v3};
                                                Arrays.sort(sortedVertexNames, Vertex::compareTo);
                                                String str = String.join("", Arrays.toString(sortedVertexNames));
                                                triangles.put(str, sortedVertexNames);
                                                break;
                                            }

                                        }
                                        a3 = a3.next;
                                    }
                                }
                            }
                            a2 = a2.next;
                        }
                    }
                    a = a.next;
                }
                v1 = v1.next;
            }
            return triangles;
        }

        /**
         * Main method for triangle graph creation. Checks the original graph for triangles.
         * Saves the vertex combinations and creates a graph based on the vertexes.
         *
         * @param name graph name
         * @return new triangle based graph
         */
        public Graph findCliques(String name) {

            HashMap<String, Vertex[]> triangles = findTriangleSets();

            HashSet<Vertex> uniqueVertexes = createUniqueIdList(triangles);

            return createCliquesGraph(name, uniqueVertexes, triangles);

        }
    }
}


//https://en.wikipedia.org/wiki/Clique_(graph_theory)
//https://www.geeksforgeeks.org/number-of-triangles-in-directed-and-undirected-graphs/
//https://enos.itcollege.ee/~jpoial/algoritmid/graafid.html
//https://www.geeksforgeeks.org/find-all-cliques-of-size-k-in-an-undirected-graph/
//https://en.wikipedia.org/wiki/Bron%E2%80%93Kerbosch_algorithm
//https://math.stackexchange.com/questions/3274108/how-to-find-all-triangles-in-a-graph
//https://www.geeksforgeeks.org/number-of-triangles-in-a-undirected-graph/
//https://enos.itcollege.ee/~jpoial/algoritmid/graafid.html
//https://stackoverflow.com/questions/23079003/how-to-convert-a-java-8-stream-to-an-array
//https://stackoverflow.com/questions/1066589/iterate-through-a-hashmap